# -*- coding: utf-8 -*-
# Copyright (c) 2015, ahmad ragheb and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class solfa(Document):
	def on_submit(self):
		Salary_slip_based_on_name= frappe.get_all("Salary Slip",filters={"employee_name":self.employee_name})

		salary_slip_doc =frappe.get_doc("Salary Slip",Salary_slip_based_on_name[0].name)

		deductions_child_table = salary_slip_doc.get("deductions")


		deductions_child_table.append({
			"salary_component": "solfa",
			"amount": self.amount
		})
		salary_slip_doc.set("deductions", deductions_child_table)
		salary_slip_doc.flags.ignore_permissions = 1
		salary_slip_doc.save()

	def before_insert(self):
		Salary_slip_based_on_name = frappe.get_all("Salary Slip", filters={"employee_name": self.employee_name})

		salary_slip_doc = frappe.get_doc("Salary Slip", Salary_slip_based_on_name[0].name)

		deductions_child_table = salary_slip_doc.get("deductions")
		earnings_child_table = salary_slip_doc.get("earnings")[0].amount
		deductions_amount = self.amount

		if float(deductions_amount) > float(earnings_child_table):
			msg = ("Error : Your Salary Basic Amount is {0} and its less than your Advance Payment Amount")
			frappe.throw(msg.format(earnings_child_table))





